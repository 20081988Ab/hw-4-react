import ProductsContainer from "../../components/ProductsContainer";
import PropTypes from "prop-types";

const Favourites = ({
  products = [],
  handleAddToFavourite = () => {},
  openSecondModalHandler,
  favoriteProducts = [],
}) => {
  return (
    <ProductsContainer
      products={products}
      handleAddToFavourite={handleAddToFavourite}
      openSecondModalHandler={openSecondModalHandler}
      favoriteProducts={favoriteProducts}
    />
  );
};

Favourites.propTypes = {
  products: PropTypes.array,
  handleAddToFavourite: PropTypes.func,
  openSecondModalHandler: PropTypes.func.isRequired,
  favoriteProducts: PropTypes.array,
};

export default Favourites;
