import styles from "./CartContainer.module.scss";
import PropTypes from "prop-types";
import CartItem from "../../components/CartItem";
import NoItemsInCart from '../../components/NoItemsInCart'


const CartContainer = ({
  cart = [],
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  deleteCartItem = () => {},
  setIsOpenFirstModal = () => {},
  setChosenCartProduct = () => {},
}) => {
  const totalPrice = cart.reduce((acc, el) => acc + el.price * el.count, 0);

  return (
    <>
      {cart.length >= 1 && (
        <div className={styles.container}>
          {cart.map((product) => (
            <CartItem
              key={`${product.id}-${Math.floor(Math.random() * 90) + 1000}`}
              product={product}
              decrementCartItem={decrementCartItem}
              incrementCartItem={incrementCartItem}
              deleteCartItem={deleteCartItem}
              setIsOpenFirstModal={setIsOpenFirstModal}
              setChosenCartProduct={setChosenCartProduct}
            />
          ))}

          <span className={styles.totalPrice}>
            {" "}
            Загальна ціна: {totalPrice} ₴{" "}
          </span>
        </div>
      )}

      {cart.length === 0 && <NoItemsInCart />}
    </>
  );
};

CartContainer.propTypes = {
  cart: PropTypes.array,
  decrementCartItem: PropTypes.func,
  incrementCartItem: PropTypes.func,
  deleteCartItem: PropTypes.func,
  setIsOpenFirstModal: PropTypes.func,
  setChosenCartProduct: PropTypes.func,
};

export default CartContainer;
