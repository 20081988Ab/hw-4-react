import styles from "./Header.module.scss";
import CartSvg from "../svg/CartSvg";
import IsFavouriteCounter from "../IsFavouriteCounter";
import PropTypes from "prop-types";

import HomeSvg from "../svg/HomeSvg";

const Header = ({
  countFavouriteProducts,
  totalCount = 0,
}) => {
  return (
    <header className={styles.header}>
      <div className={styles.container}>
        <a className={styles.shopName} href="#">
          Parfume shop
        </a>

        <nav className={styles.cartContainer}>
          <HomeSvg />
          <IsFavouriteCounter countFavouriteProducts={countFavouriteProducts} />
          <CartSvg
            totalCount={totalCount}
          />
        </nav>
      </div>
    </header>
  );
};

Header.propTypes = {
  countFavouriteProducts: PropTypes.func.isRequired,
  totalCount: PropTypes.number,
};

export default Header;
