import PropTypes from "prop-types";

import Modal from "..";

const ModalWrapper = ({
  text,
  bodyText,
  className,
  hasImage,
  imagePath = '',
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart = false,
  thirdText,
  modalIsOpen,
  handleCloseModal = () => {},
  countAddedToCartProducts = () => {},
  productId = 0,
  addToCart = () => {},
  deleteCartItem = () => {},
  totalCount,
}) => {
  return (
    <div className="modalWrapper">
      <Modal
        text={text}
        bodyText={bodyText}
        className={className}
        hasImage={hasImage}
        imagePath={imagePath}
        footerCancelBtn={footerCancelBtn}
        firstText={firstText}
        footerDeleteBtn={footerDeleteBtn}
        secondText={secondText}
        footerAddToCart={footerAddToCart}
        thirdText={thirdText}
        modalIsOpen={modalIsOpen}
        handleCloseModal={handleCloseModal}
        countAddedToCartProducts={countAddedToCartProducts}
        productId={productId}
        addToCart={addToCart}
        deleteCartItem={deleteCartItem}
        totalCount={totalCount}
      />
    </div>
  );
};

ModalWrapper.propTypes = {
  text: PropTypes.string.isRequired,
  bodyText: PropTypes.string.isRequired,
  className: PropTypes.string,
  hasImage: PropTypes.oneOf(["true", "false"]).isRequired,
  imagePath: PropTypes.string,
  footerCancelBtn: PropTypes.string,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.string,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.bool,
  footerAddToFavBtn: PropTypes.string,
  thirdText: PropTypes.string,
  modalIsOpen: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func,
  countAddedToCartProducts: PropTypes.func,
  productId: PropTypes.number,
  addToCart: PropTypes.func,
  deleteCartItem: PropTypes.func,
  totalCount: PropTypes.number,
};

export default ModalWrapper;
