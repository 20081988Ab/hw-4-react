import PropTypes from "prop-types";
import ModalImage from "./ModalImage";
import ModalHeader from "./ModalHeader";
import ModalClose from "./ModalClose";
import ModalBody from "./ModalBody";
import ModalFooter from "./ModalFooter";
import ModalBackground from "./ModalBackground";

const Modal = ({
  text,
  className,
  bodyText,
  hasImage,
  imagePath,
  footerCancelBtn,
  firstText,
  footerDeleteBtn,
  secondText,
  footerAddToCart = false,
  thirdText,
  modalIsOpen,
  handleCloseModal,
  countAddedToCartProducts,
  productId = 0,
  addToCart = () => {},
  deleteCartItem = () => {},
  totalCount,
  
}) => {
  
  return (
    <>
      <div className={className}>
        {hasImage === "true" && <ModalImage imagePath={imagePath}/>}
        <ModalHeader>{text}</ModalHeader>
        <ModalClose handleCloseModal={handleCloseModal} />
        <ModalBody bodyText={bodyText} />
        <ModalFooter
          footerCancelBtn={footerCancelBtn}
          firstText={firstText}
          footerDeleteBtn={footerDeleteBtn}
          secondText={secondText}
          footerAddToCart={footerAddToCart}
          thirdText={thirdText}
          modalIsOpen={modalIsOpen}
          firstClick={() => {}}
          secondaryClick={() => {}}
          handleCloseModal={handleCloseModal}
          countAddedToCartProducts={countAddedToCartProducts}
          productId={productId}
          addToCart={addToCart}
          deleteCartItem={deleteCartItem}
          totalCount={totalCount}
        />
      </div>

      <ModalBackground handleCloseModal={handleCloseModal} />
    </>
  );
};

Modal.propTypes = {
  text: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  bodyText: PropTypes.string.isRequired,
  hasImage: PropTypes.oneOf(["true", "false"]).isRequired,
  imagePath: PropTypes.string.isRequired,
  footerCancelBtn: PropTypes.string,
  firstText: PropTypes.string,
  footerDeleteBtn: PropTypes.string,
  secondText: PropTypes.string,
  footerAddToCart: PropTypes.bool,
  footerAddToFavBtn: PropTypes.string,
  thirdText: PropTypes.string,
  modalIsOpen: PropTypes.func.isRequired,
  handleCloseModal: PropTypes.func.isRequired,
  countAddedToCartProducts: PropTypes.func.isRequired,
  productId: PropTypes.number.isRequired,
  addToCart: PropTypes.func,
  deleteCartItem: PropTypes.func,
  totalCount: PropTypes.number,
};

export default Modal;
