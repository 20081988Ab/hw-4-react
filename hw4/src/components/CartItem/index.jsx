import styles from "./CartItem.module.scss";
import Button from "../Button";
import DeleteSvg from "../svg/DeleteSvg";

import PropTypes from "prop-types";

const CartItem = ({
  product,
  decrementCartItem = () => {},
  incrementCartItem = () => {},
  setIsOpenFirstModal = () => {},
  setChosenCartProduct = () => {},
}) => {
  const { id, imagePath, name, price, count } = product;

  const totalPrice = price * count;

  return (
    <div className={styles.itemContainer}>
      <img src={imagePath} className={styles.image} alt="" />

      <div className={styles.addInfo}>
        <span>{name}</span>

        <span>Price: {price} ₴ </span>
      </div>

      <div className={styles.price}>
        <span>Total: {count} </span>

        <div className={styles.countButtonsContainer}>
          <Button
            className={styles.countButton}
            onClick={() => {
              incrementCartItem(id);
            }}
          >
            +
          </Button>

          <Button
            count={count}
            className={styles.countButton}
            onClick={() => {
              decrementCartItem(id);
            }}
          >
            -
          </Button>
        </div>

        <span>Total price: {totalPrice} ₴ </span>
      </div>

      <div className={styles.buttonContainer}>
        <Button
          className={styles.deleteButton}
          onClick={() => {
            setIsOpenFirstModal(true);
            setChosenCartProduct(product);
          }}
        >
          <DeleteSvg />
        </Button>
      </div>
    </div>
  );
};

CartItem.propTypes = {
  product: PropTypes.shape({
    id: PropTypes.number.isRequired,
    imagePath: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    price: PropTypes.number.isRequired,
    color: PropTypes.string.isRequired,
    artNumber: PropTypes.string.isRequired,
    count: PropTypes.number.isRequired,
  }).isRequired,
  decrementCartItem: PropTypes.func.isRequired,
  incrementCartItem: PropTypes.func.isRequired,
  setIsOpenFirstModal: PropTypes.func.isRequired,
  setChosenCartProduct: PropTypes.func.isRequired,
};

export default CartItem;
